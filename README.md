# Projecte Final ASIXc

## Projecte Final del CFGS ASIXc

### Proposta de projecte:

Creació d’un domini ldap, per la implementació a l’institut.


### Integrants del grup:

Jiménez Mármol, Oriol

Mellado Tobenya, Guillem

Del pozo Martinez, Adrià

Laaziz Laziz, Youssef


### Descripció general del projecte:

La idea és crear un domini per a màquines Linux i Windows, de manera que un cop acabat es pugui implementar a l’institut. Per a això utilitzarem la tecnologia SAMBA per a la compartició de fitxers entre màquines. També inclourem còpies de seguretat automàtiques per a garantir l’accés a les dades. Per últim farem servir Docker per a un més fàcil desplegament.


### Tecnologies utilitzades:

- SAMBA-AD-DC

- PAM 

- Samba

- Linux

- Windows

- Clonezilla

- Rsync
