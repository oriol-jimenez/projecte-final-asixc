#!/bin/bash
sortir=0
while [ $sortir -eq 0 ];do
echo ""
echo -e "\e[96m**** AFEGIR USUARI ****\e[0m"
echo "[1] Afegir usuari manualment"
echo "[2] Afegir usuari a partir de fitxer csv"
echo "[3] Sortir"
echo "Escull una opció:"
read opcio
echo ""
if [ "$opcio" == "1" ]
then
	echo "-- Afegir usuari manualment --"
	echo ""
	echo "Nom:"
	read nom
	echo "Cognom:"
	read cognom
	echo ""
	user=`echo "$nom.$cognom" | tr [A-Z] [a-z]`
	password="ITB_2020"
	existeix=`samba-tool user list | grep $user | wc -l`
	if [ $existeix -ne 0 ]
	then
		cont=2
		user2="$user$cont"
		existeix2=`samba-tool user list | grep $user2 | wc -l`
		while [ $existeix2 -ne 0 ];do
			cont=$((cont+1))
			user2="$user$cont"
			existeix2=`samba-tool user list | grep $user2 | wc -l`
		done
			echo ""
			echo "L'usuari $user ja existeix, canviant el nom a $user2..."
			user="$user2"
	fi
	mail=`echo "$user@itb.org" | tr [A-Z] [a-z]`
	echo ""
	samba-tool user create $user $password --use-username-as-cn --given-name=$nom --surname=$cognom --mail-address=$mail --login-shell=/bin/bash
	if [ $? -eq 0 ]
	then
		echo "Nom d'usuari: $user"
		echo "Correu: $mail"
		echo ""
		net sam set pwdmustchangenow $user yes
		echo -e "La contrasenya per defecte és \e[92mITB_2020\e[0m"
		echo "L'usuari haurà de canviar la contrasenya quan iniciï sessió per primer cop."
		echo ""
	fi
elif [ "$opcio" == "2" ]
then
	echo "-- Afegir usuari a partir de fitxer csv --"
	echo ""
	echo "Recorda que el fixter ha de tenir els següents camps separats per espais: Nom Cognom"
	echo "Indica la ruta del fitxer a llegir:"
	read fitxer
	echo ""
	sed 1d $fitxer > output.csv
	while read line
	do
		echo ""
		password="ITB_2020"
		nom=`echo $line | cut -d ' ' -f1`
		cognom=`echo $line | cut -d ' ' -f2`
		user=`echo $nom.$cognom | tr [A-Z] [a-z]`
		existeix=`samba-tool user list | grep $user | wc -l`
		if [ $existeix -ne 0 ]
		then
			cont=2
			user2="$user$cont"
			existeix2=`samba-tool user list | grep $user2 | wc -l`
			while [ $existeix2 -ne 0 ];do
				cont=$((cont+1))
				user2="$user$cont"
				existeix2=`samba-tool user list | grep $user2 | wc -l`
			done
			echo ""
			echo "L'usuari $user ja existeix, canviant el nom a $user2..."
			user="$user2"
		fi
		mail="$user@itb.org"
		samba-tool user create $user $password --use-username-as-cn --given-name=$nom --surname=$cognom --mail-address=$mail --login-shell=/bin/bash
		if [ $? -eq 0 ]
		then
			echo "Nom d'usuari: $user"
			echo "Correu: $mail"
			net sam set pwdmustchangenow $user yes
		fi
	done < output.csv
	rm output.csv
	echo ""
	echo -e "La contrasenya per defecte és \e[92mITB_2020\e[0m"
	echo "Els usuaris hauran de canviar la contrasenya quan iniciïn sessió per primer cop."
	echo ""
elif [ "$opcio" == "3" ]
then
	echo -e "\e[31mSortint...\e[0m"
	sortir=1
else
	echo "Opció incorrecta."
fi
done
