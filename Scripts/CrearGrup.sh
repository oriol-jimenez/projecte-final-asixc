#!/bin/bash
sortir=0
while [ $sortir -eq 0 ];do
echo ""
echo -e "\e[96m**** CREAR GRUP ****\e[0m"
echo "[1] Crear grup manualment"
echo "[2] Crear grup a partir de fitxer csv"
echo "[3] Sortir"
echo "Escull una opció:"
read opcio
echo ""
if [ "$opcio" == "1" ]
then
	echo "-- Crear Grup manualment --"
	echo ""
	echo "Introdueix el nom del grup que vulguis crear:"
	read grup
	samba-tool group add $grup
	if [ $? -eq 0 ]
	then
		echo "Grup $group creat correctament"
	fi
	
elif [ "$opcio" == "2" ]
then
	echo "-- Crear Grup a partir de fitxer csv --"
	echo ""
	echo "Recorda que el fixter ha de tenir el següent camp a cada línia: Grup"
	echo "Indica la ruta del fitxer a llegir:"
	read fitxer
	echo ""
	sed 1d $fitxer > output.csv
	while read line
	do
		echo ""
		grup=`echo $line | cut -d ' ' -f1`
		samba-tool group add $grup
		
		if [ $? -eq 0 ]
		then
			echo "Grup $group creat correctament"
		fi
	done < output.csv
	rm output.csv

elif [ "$opcio" == "3" ]
then
	echo -e "\e[31mSortint...\e[0m"
	sortir=1
else
	echo "Opció incorrecta."
fi
done