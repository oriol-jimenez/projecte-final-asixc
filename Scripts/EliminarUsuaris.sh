#!/bin/bash
sortir=0
while [ $sortir -eq 0 ];do
echo ""
echo -e "\e[96m**** ESBORRAR USUARI ****\e[0m"
echo "[1] Esborrar usuari manualment"
echo "[2] Esborrar usuari a partir de fitxer csv"
echo "[3] Sortir"
echo "Escull una opció:"
read opcio
echo ""
if [ "$opcio" == "1" ]
then
	echo "-- Esborrar usuari manualment --"
	echo ""
	echo "Nom d'usuari:"
	read user
	echo ""
	samba-tool user delete $user
	
	if [ $? -eq 0 ]
	then
		echo "L'usuari $user ha estat eliminat correctament"
		rm -r /home/ITB/$user
	fi
elif [ "$opcio" == "2" ]
then
	echo "-- Esborrar usuari a partir de fitxer csv --"
	echo ""
	echo "Recorda que el fixter ha de tenir un nom d'usuari a cada línia"
	echo "Indica la ruta del fitxer a llegir:"
	read fitxer
	echo ""
	sed 1d $fitxer > output.csv
	while read line
	do
		echo ""
		user=`echo $line | cut -d ' ' -f1`
		samba-tool user delete $user
	
		if [ $? -eq 0 ]
		then
			echo "L'usuari $user ha estat eliminat correctament."
			rm -r /home/ITB/$user
		fi
	done < output.csv
	rm output.csv
	
elif [ "$opcio" == "3" ]
then
	echo -e "\e[31mSortint...\e[0m"
	sortir=1
else
	echo "Opció incorrecta."
fi
done
