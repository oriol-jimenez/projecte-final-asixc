#!/bin/bash
sortir=0
while [ $sortir -eq 0 ];do
echo ""
echo -e "\e[96m**** CANVIAR CONTRASENYA ****\e[0m"
echo "[1] Canviar contrasenya manualment"
echo "[2] Canviar contrasenya a partir de fitxer csv"
echo "[3] Sortir"
echo "Escull una opció:"
read opcio
echo ""
if [ "$opcio" == "1" ]
then
	echo "-- Canviar contrasenya manualment --"
	echo ""
	echo "Introdueix el nom del usuari per canviar contrasenya:"
	read user
	samba-tool user setpassword $user
	
elif [ "$opcio" == "2" ]
then
	echo "-- Canviar contrasenya a partir de fitxer csv --"
	echo ""
	echo "Recorda que el fixter ha de tenir el següents camps separats amb espais: Usuari Contrasenya"
	echo "Indica la ruta del fitxer a llegir:"
	read fitxer
	echo ""
	sed 1d $fitxer > output.csv
	while read line
	do
		echo ""
		usuari=`echo $line | cut -d ' ' -f1`
		password=`echo $line | cut -d ' ' -f2`
		samba-tool user setpassword $usuario --newpassword $password		

		if [ $? -eq 0 ]
		then
			echo "La contrasenya de $usuari ha canviat a $password"
		fi
	done < output.csv
	rm output.csv

elif [ "$opcio" == "3" ]
then
	echo -e "\e[31mSortint...\e[0m"
	sortir=1
else
	echo "Opció incorrecta."
fi
done