#!/bin/bash
sortir=0
while [ $sortir -eq 0 ];do
echo ""
echo -e "\e[96m**** AFEGIR USUARI A GRUP ****\e[0m"
echo "[1] Afegir usuari a grup manualment"
echo "[2] Afegir usuari a grup a partir de fitxer csv"
echo "[3] Sortir"
echo "Escull una opció:"
read opcio
echo ""
if [ "$opcio" == "1" ]
then
	echo "-- Afegir usuari a grup manualment --"
	echo ""
	echo "Introdueix el nom del usuari:"
	read usuari
	echo "Introdueix el nom del grup:"
	read grup
	samba-tool group addmembers $grup $usuari
	
elif [ "$opcio" == "2" ]
then
	echo "-- Afegir usuari a grup a partir de fitxer csv --"
	echo ""
	echo "Recorda que el fixter ha de tenir el següents camps separats amb espais: Usuari Grup"
	echo "Indica la ruta del fitxer a llegir:"
	read fitxer
	echo ""
	sed 1d $fitxer > output.csv
	while read line
	do
		echo ""
		usuari=`echo $line | cut -d ' ' -f1`
		grup=`echo $line | cut -d ' ' -f2`
		samba-tool group addmembers $grup $usuari		

		if [ $? -eq 0 ]
		then
			echo "Usuari $usuari afegit a grup $grup"
		fi
	done < output.csv
	rm output.csv

elif [ "$opcio" == "3" ]
then
	echo -e "\e[31mSortint...\e[0m"
	sortir=1
else
	echo "Opció incorrecta."
fi
done