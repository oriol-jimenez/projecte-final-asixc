# Ús dels scripts

### AfegirUsuaris.sh
Aquest script permet afegir usuaris al domini, ja sigui indicant nom i cognom manualment, o llegint un fitxer csv amb noms i cognoms de tots els usuaris.
Primer de tot mostra un menú amb l’opció d’afegir un usuari manualment, si vols afegir usuaris a partir d’un fitxer csv o si vols sortir de l’script. 
En l’opció manual, has d’introduir nom i cognom, i el mateix script ja genera el nom d’usuari, el correu i afegeix l’usuari al domini amb una contrasenya predeterminada (ITB_2020), que haurà de canviar el proper inici de sessió.
En l’opció de llegir un fitxer csv, l’script llegeix línia a línia el fitxer, que ha de contenir nom i cognom, i fa el mateix que amb l’opció manual, genera automàticament l’usuari, el correu i la contrasenya. Aquest procés el repeteix fins que arriba a l’última línia, on finalment para. 
Hem afegit un extra per aquest script, i és que en cas que es generi un usuari que ja existeix, es comprova abans d’afegir-lo si existeix un usuari amb el mateix nom, i si existeix, llavors li afegeix un 2 al final. Si l’usuari amb un 2 també existeix, li posa un 3, i així successivament, per evitar que hi hagi conflictes amb els noms d’usuari.
Per últim, un cop afegeix els usuaris al domini, torna a mostrar el menú per si volem tornar a afegir usuaris. En cas contrari, només hem de seleccionar l’opció de sortir, i l’script acabarà.

### EliminarUsuaris.sh
Aquest script permet eliminar usuaris del domini, ja sigui indicant el nom d’usuari de manera manual, o llegint un fitxer csv.
Quan executem aquest script veiem un menú amb tres opcions, que són la d’eliminar un usuari manualment, eliminar usuaris a partir d’un fitxer csv i la de sortir del script.
En l’opció manual, s’ha d'introduir el nom d’usuari que volem eliminar, i el script l’elimina del domini.
En l’opció de llegir el fitxer csv, l’script llegeix línia a línia el fitxer, i va esborrant tots els usuaris del domini, fins que arriba a l’última línia, on finalitza el procés.
A més, quan s’elimina un usuari, també esborrem la seva carpeta home, per a que no creï conflicte si després afegim un usuari amb el mateix nom. D’aquesta manera suprimir el conflicte.
Per últim, un cop elimina els usuaris del domini, torna a mostrar el menú per si volem tornar a eliminar usuaris. En cas contrari, només hem de seleccionar l’opció de sortir, i l’script acabarà.

### ActivarUsuaris.sh
Aquest script ens permet activar els usuaris del domini, ja sigui de manera manual o llegint un fitxer csv.
Quan executem l’script, es mostra un menú amb tres opcions. La primera és si volem activar un usuari manualment, la qual ens preguntarà el nom d’usuari que volem activar. D’aquesta manera, l’script agafarà aquest usuari i l’activarà.
La segona opció és la de llegir els usuaris amb un fitxer csv. En aquesta opció ens pregunta quin fitxer volem fer servir, i nosaltres hem de posar la ruta del fitxer. D’aquesta manera el llegirà línia per línia i anirà activant un a un tots els usuaris que trobi, fins la última línia de l’arxiu, on sortirà del bucle.
Per últim, un cop ha activat els usuaris del domini, torna a mostrar el menú per si volem activar més usuaris. En cas contrari, només hem de seleccionar la tercera opció, que és la de sortir, i l’script acabarà.

### DesactivarUsuaris.sh
Aquest script ens permet desactivar els usuaris del domini, ja sigui de manera manual o llegint un fitxer csv.
Quan executem l’script, es mostra un menú amb tres opcions. La primera és si volem desactivar un usuari manualment, la qual ens preguntarà el nom d’usuari que volem desactivar. D’aquesta manera, l’script agafarà aquest usuari i el desactivarà.
La segona opció és la de llegir els usuaris amb un fitxer csv. En aquesta opció ens pregunta quin fitxer volem fer servir, i nosaltres hem de posar la ruta del fitxer. D’aquesta manera el llegirà línia per línia i anirà desactivant un a un tots els usuaris que trobi, fins la última línia de l’arxiu, on sortirà del bucle.
Per últim, un cop ha desactivat els usuaris del domini, torna a mostrar el menú per si volem desactivar més usuaris. En cas contrari, només hem de seleccionar la tercera opció, que és la de sortir, i l’script acabarà.

### ModificarPassword.sh
Aquest script permet canviar la contrasenya tant d’un usuari com d’un conjunt d’ells, ja que pots introduir l’usuari manualment o pot llegir un fitxer csv per a canviar la contrasenya de molts alumnes alhora.
Quan executem aquest script, el primer que se’ns mostra és un menú amb tres opcions. La primera és per introduir un usuari manualment al qual volem canviar la contrasenya. Si escollim aquesta opció, ens demanarà un usuari i una contrasenya. Així doncs podem canviar la contrasenya d’un sol usuari.
Si volem canviar la contrasenya a un grup d’usuaris, els haurem de ficar en un fitxer csv, un usuari a cada línia i seleccionar la segona opció del menú d’aquest script. Ens demanarà la ruta del fitxer que volem utilitzar i la contrasenya que volem establir, i començarà a llegir línia per línia, canviant la contrasenya a cadascun dels usuaris que estiguin al fitxer, fins la última línia, que deixarà de llegir el fitxer.
Per últim, un cop s’ha canviat la contrasenya dels usuaris que voliem, ens apareix un altre cop el menú per si volem canviar més contrasenyes. En cas contrari, només hem de seleccionar la tercera opció, que és la de sortir, i l’script acabarà.
    
### LlistarUsuaris.sh
Aquest script és ben simple, ja que només mostra els usuaris que tenim al domini. Ens pot venir bé alhora de voler saber si un usuari es troba o no dins del domini.
El funcionament és molt bàsic, ja que només executa una comanda nativa de **samba-tool** que permet llistar els usuaris del domini. Tot i això hem cregut adient crear aquest script per evitar haver de recordar-nos de la comanda i així fer més eficient i ràpida la cerca d’un usuari.
A més, si combinem aquest script amb l’eina **grep**, podem buscar un usuari concret.

### CrearGrup.sh
Aquest script ens permet crear grups per al nostre domini ja sigui manualment o a partir d’un fitxer csv amb un grup per fila.
Quan executem l’script, se’ns mostra un menú amb tres opcions. 
La primera és per introduir el nom d’un grup de manera manual, per si només volem afegir un grup en aquell moment. A l’introduir el nom, l’script executa la comanda de creació de grups amb el nom indicat. 
La segona opció permet la creació de molts grups alhora, simplement posant el nom dels grups en un fitxer csv, un per línia. Al seleccionar aquesta segona opció, l’script ens demana la ruta del fitxer que volem fer servir. D’aquesta manera s’anirà executant automàticament la comanda per crear grups amb tot el contingut del fitxer, fins que arribi a la última línia, on deixarà d’executar-se.
Per últim, un cop s’han creat tots els grups que volem, ens apareix un altre cop el menú per si volem crear més grups. En cas contrari, només hem de seleccionar la tercera opció, que és la de sortir, i l’script acabarà.

### EliminarGrup.sh
Aquest script té una funció molt simple, i no és una altra que la d’eliminar un grup del domini. Això sí, ens permet eliminar un grup manualment o un conjunt de grups llegint un fitxer csv.
Quan executem l’script, podem observar un menú amb tres opcions. La primera d’elles és la d’introduir un grup manualment. D’aquesta manera el script ens demana el nom del grup i simplement l’elimina.
La segona opció, una mica més complexa, és la d’eliminar grups mitjançant un fitxer csv. En aquest cas haurem d’introduir la ruta del fitxer que volem fer servir. Aquest arxiu haurà de contenir el nom d’un grup a cada línia, de manera que es llegirà línia per línia i s’anirà eliminant un a un.
Finalment, quan ja s’han esborrat tots els grups, tornarem a visualitzar el menú, ja que està posat en bucle. Podem tornar a eliminar grups si volem, o per el contrari, podem seleccionar la tercera opció i sortir de l’script.

### MembresGrup.sh
Aquest script ens permet mostrar tots els membres d’un grup concret.
A diferència dels scripts anteriors, aquest no mostra cap menú, sino que directament ens preguntarà el grup del qual volem saber els components. Tot i això, hem posat un bucle per a que un cop s’hagin mostrat els membres del grup, ens torni a preguntar un altre grup, per fer més còmode la visualització de membres de diversos grups. Per a poder sortir de l’script, només haurem d’escriure la paraula “fi” i en aquest moment finalitzarà.

### LlistarGrups.sh
Aquest script és ben simple, ja que només mostra els grups que tenim al domini. Ens pot venir bé alhora de voler saber si existeix o no un grup en el domini.
El funcionament és molt bàsic, ja que només executa una comanda nativa de **samba-tool** que permet llistar els grups del domini. Tot i això hem cregut adient crear aquest script per evitar haver de recordar-nos de la comanda i així fer més eficient i ràpida la cerca d’un grup.
A més, si combinem aquest script amb l’eina **grep**, podem buscar un grup concret.

### AfegirUsuarisAGrups.sh
Aquest script és un dels més útils que tenim, ja que permet afegir usuaris a un grup ja sigui de manera manual o llegint un fitxer csv. Un exemple en el qual pot ser molt útil aquest script és quan comencem un curs nou i s’han de crear els grups dels nous alumnes que entren. Amb aquest script podem indicar la ruta d’un fitxer csv que contingui tots els usuaris dels nous alumnes i el grup al qual els volem afegir, i l’script farà la resta, ja que els ficarà a tots automàticament sense haver d’estar escrivint comandes mil cops.
El funcionament en sí és senzill. Quan executem l’script se’ns mostra un menú amb tres opcions. La primera és per a afegir un usuari a un grup manualment, per exemple en el cas que s’apunti un alumne a meitat de curs i l’haguem de ficar en el grup corresponent. Indiquem l’usuari i el grup.
La segona opció és la de llegir un fitxer csv amb un usuari per línia i escollir un grup on ficar tots aquests usuaris. D’aquesta manera l’script anirà llegint línia per línia el fitxer i anirà afegint els usuaris al grup seleccionat.
Un cop afegits tots els usuaris als grups que els pertoquen, es mostra un altre cop el menú principal per si volem afegir qualsevol usuari més. En cas contrari, podem seleccionar la tercera opció, que és la de sortir, per fer que l’script finalitzi.

### EliminarUsuarisDeGrups.sh
Aquest script té la finalitat contrària a l’anterior, ja que permet eliminar usuaris d’un grup, ja sigui de manera manual o llegint un fitxer csv.
Quan executem l’script es mostra un menú on veiem tres opcions. La primera opció permet eliminar un usuari d’un grup de manera manual i d’un en un.
La segona opció et demana la ruta a un fitxer csv on a cada línia hi hagi un usuari. L’script llegirà el fitxer línia per línia i anirà eliminant els usuaris del grup que haguem escollit.
Un cop eliminats tots els usuaris del grup corresponent, es mostra un altre cop el menú principal per si volem tornar a repetir alguna operació. En cas contrari podem seleccionar la tercera opció, que ens permet finalitzar l’script.

### CrearCopiasSeguretatManual.sh
Aquest script té la finalitat de generar còpies de seguretat de forma manual. La primera opció, còpia per defecte tots els fitxers d'un directori d'origen a un directori de destí, la segona és com la primera opció però també permet fer còpies perfectes de carpetes i directoris complets. La tercera realitza una còpia de seguretat en la qual s'exclou arxius amb un format determinat. La cuarta és com la tercera però en lloc de tenir en compte l’extensió té en compte la mida per fer les còpies de seguretat.
